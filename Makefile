include color-makefile/colors.mk

CFLAGS += -DNOUI -DVERSION=1 -MMD

all: cclargs

# MAIN PROGRAM
cclargs: cclargs_lite.o main.o
	$(call make_echo_link_c_executable)
	$(at) gcc $^ -o $@
	$(at) echo "Built target $@"


# TESTING TARGETS
check: check_unittest check_output check_completion check_pycompletion check_gencompletion

# Unit tests for functions in cclargs_lite.c
check_unittest: run_test
	$(call make_echo_run_test,"Running unit test executable $<")
	$(at) ./$<
	$(call success)
run_test: cclargs_lite.o test.o
	$(call make_echo_link_c_executable)
	$(at) gcc $^ -o $@
	$(at) echo "Built target $@"

# Regression test comparing output for ord_soumet with reference
check_output: output.sh
	$(call make_echo_run_test,"Comparing $< to reference file")
	$(at) diff $< ref_output.sh
	$(call success)
output.sh: cclargs
	$(call make_echo_generate_file)
	$(at) ./ord_soumet_cclargs_call.sh > $@ 2>/dev/null

# Regression test comparing autocomplete for ord_soumet with reference
check_completion: ord_soumet_completion.bash
	$(call make_echo_run_test,"Comparing $< to reference file")
	$(at) diff $< ref_completion.bash
	$(call success)
ord_soumet_completion.bash: ord_soumet_cclargs_call.sh cclargs
	$(call make_echo_generate_file)
	$(at) ./$< -generate-autocomplete 2>$@ >/dev/null
check_pycompletion: ord_soumet_pycompletion.bash
	$(call make_echo_run_test,"Comparing $< to reference file")
	$(at) diff $< ref_completion.bash
	$(call success)
ord_soumet_pycompletion.bash: cclargs-completion-adapter.py cclargs
	$(call make_echo_generate_file)
	$(at) PATH=$(PWD):$(PATH) ./$< ord_soumet 2>$@ >/dev/null

check_gencompletion: generated_completion.bash
	$(call make_echo_run_test,"Comparing $< to reference file")
	$(at) diff $< ref_gencompletion.bash
	$(call success)
generated_completion.bash: autocomp_generator.py options.txt
	$(call make_echo_generate_file)
	$(at) python3 $< -o options.txt 2>$@


# DEmonstration relsating to '*pttmp--'
.PHONY: smm
smm: star_minus_minus.o
	$(call make_echo_link_c_executable)
	$(at) gcc $< -o $@
check_smm: smm
	$(call make_echo_run_test,"Running demo of '*pttmp--'")
	./$<

# COMPILATION RULES
%.o: %.c
	$(call make_echo_build_c_object)
	$(at) gcc -c $(CFLAGS) $< -o $@

# Cleanup
clean:
	$(at) rm -f *.d *.o cclargs smm output.sh run_test ord_soumet_completion.bash ord_soumet_pycompletion.bash generated_completion.bash

-include *.d
