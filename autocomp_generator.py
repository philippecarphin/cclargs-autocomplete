import argparse
import sys

p = argparse.ArgumentParser()
p.add_argument("--options-file", "-o", help="Options file")
args = p.parse_args()

scriptnom = 'ord_soumet'

start = """#!/bin/bash

# This is the function that will be called when we press TAB.
#
# It's purpose is # to examine the current command line (as represented by the
# array COMP_WORDS) and to determine what the autocomplete should reply through
# the array COMPREPLY.
#
# This function is organized with subroutines who  are responsible for setting
# the 'candidates' variable.
#
# The compgen then filters out the candidates that don't begin with the word we are
# completing. In this case, if '--' is one of the words, we set empty candidates,
# otherwise, we look at the previous word and delegate # to candidate-setting functions
__complete_{scriptnom}() {{

	COMPREPLY=()

	# We use the current word to filter out suggestions
	local cur="${{COMP_WORDS[COMP_CWORD]}}"

	# Compgen: takes the list of candidates and selects those matching ${{cur}}.
	# Once COMPREPLY is set, the shell does the rest.
	COMPREPLY=( $(compgen -W "$(__suggest_{scriptnom}_candidates)" -- ${{cur}}))

	return 0
}}

__suggest_{scriptnom}_candidates(){{
	# We use the current word to decide what to do
	local cur="${{COMP_WORDS[COMP_CWORD]}}"
	if __{scriptnom}_dash_dash_in_words ; then
		return
	fi

	option=$(__{scriptnom}_get_current_option)
	if [[ "$option" != "" ]] ; then
		__suggest_{scriptnom}_args_for_option ${{option}}
	else
		if [[ "$cur" = -* ]] ; then
			__suggest_{scriptnom}_options
		fi
	fi

	echo "$candidates"
}}

__{scriptnom}_dash_dash_in_words(){{
	for ((i=0;i<COMP_CWORD-1;i++)) ; do
		w=${{COMP_WORD[$i]}}
		if [[ "$w" == "--" ]] ; then
			return 0
		fi
	done
	return 1
}}

__{scriptnom}_get_current_option(){{
	# The word before that
	local prev="${{COMP_WORDS[COMP_CWORD-1]}}"
	if [[ "$prev" == -* ]] ; then
		echo "$prev"
	fi
}}
"""

print(start.format(scriptnom=scriptnom), file=sys.stderr)

with open(args.options_file, 'r') as f:
    lines = f.read().splitlines()

data = {}
for l in lines:
    words = l.split()
    data[words[0]] = words[1:]


print("""__suggest_{scriptnom}_options(){{
\tcandidates=" {candidates}"
}}
""".format(scriptnom=scriptnom, candidates=' '.join([f'{o}' for o in data])), file=sys.stderr)


print("""__suggest_{}_args_for_option(){{
\tcase "$1" in""".format(scriptnom), file=sys.stderr)

for opt in data:
    print(f"""\t\t{opt}) __suggest_{scriptnom}_key_{opt.strip("-").replace("-","_")}_values ;;""", file=sys.stderr)

print("""\tesac
}
""", file=sys.stderr)


for opt in data:
    print("""__suggest_{}_key_{}_values(){{
\tcandidates="{}"
}}
""".format(scriptnom, opt.strip("-").replace("-","_"), ' '.join(data[opt])), file=sys.stderr)

print(f"complete -o default -F __complete_{scriptnom} {scriptnom}", file=sys.stderr)
