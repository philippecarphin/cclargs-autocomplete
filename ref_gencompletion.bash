#!/bin/bash

# This is the function that will be called when we press TAB.
#
# It's purpose is # to examine the current command line (as represented by the
# array COMP_WORDS) and to determine what the autocomplete should reply through
# the array COMPREPLY.
#
# This function is organized with subroutines who  are responsible for setting
# the 'candidates' variable.
#
# The compgen then filters out the candidates that don't begin with the word we are
# completing. In this case, if '--' is one of the words, we set empty candidates,
# otherwise, we look at the previous word and delegate # to candidate-setting functions
__complete_ord_soumet() {

	COMPREPLY=()

	# We use the current word to filter out suggestions
	local cur="${COMP_WORDS[COMP_CWORD]}"

	# Compgen: takes the list of candidates and selects those matching ${cur}.
	# Once COMPREPLY is set, the shell does the rest.
	COMPREPLY=( $(compgen -W "$(__suggest_ord_soumet_candidates)" -- ${cur}))

	return 0
}

__suggest_ord_soumet_candidates(){
	# We use the current word to decide what to do
	local cur="${COMP_WORDS[COMP_CWORD]}"
	if __ord_soumet_dash_dash_in_words ; then
		return
	fi

	option=$(__ord_soumet_get_current_option)
	if [[ "$option" != "" ]] ; then
		__suggest_ord_soumet_args_for_option ${option}
	else
		if [[ "$cur" = -* ]] ; then
			__suggest_ord_soumet_options
		fi
	fi

	echo "$candidates"
}

__ord_soumet_dash_dash_in_words(){
	for ((i=0;i<COMP_CWORD-1;i++)) ; do
		w=${COMP_WORD[$i]}
		if [[ "$w" == "--" ]] ; then
			return 0
		fi
	done
	return 1
}

__ord_soumet_get_current_option(){
	# The word before that
	local prev="${COMP_WORDS[COMP_CWORD-1]}"
	if [[ "$prev" == -* ]] ; then
		echo "$prev"
	fi
}

__suggest_ord_soumet_options(){
	candidates=" --day-of-week --color --flag"
}

__suggest_ord_soumet_args_for_option(){
	case "$1" in
		--day-of-week) __suggest_ord_soumet_key_day_of_week_values ;;
		--color) __suggest_ord_soumet_key_color_values ;;
		--flag) __suggest_ord_soumet_key_flag_values ;;
	esac
}

__suggest_ord_soumet_key_day_of_week_values(){
	candidates="Monday Tuesday Wednesday Thursday Friday Saturday Sunday"
}

__suggest_ord_soumet_key_color_values(){
	candidates="red green blue"
}

__suggest_ord_soumet_key_flag_values(){
	candidates=""
}

complete -o default -F __complete_ord_soumet ord_soumet
