# Changing multi-value argument delimiter from : to ~
#!/bin/bash

# This is the function that will be called when we press TAB.
#
# It's purpose is # to examine the current command line (as represented by the
# array COMP_WORDS) and to determine what the autocomplete should reply through
# the array COMPREPLY.
#
# This function is organized with subroutines who  are responsible for setting
# the 'candidates' variable.
#
# The compgen then filters out the candidates that don't begin with the word we are
# completing. In this case, if '--' is one of the words, we set empty candidates,
# otherwise, we look at the previous word and delegate # to candidate-setting functions
__complete_ord_soumet() {

	COMPREPLY=()

	# We use the current word to filter out suggestions
	local cur="${COMP_WORDS[COMP_CWORD]}"

	# Compgen: takes the list of candidates and selects those matching ${cur}.
	# Once COMPREPLY is set, the shell does the rest.
	COMPREPLY=( $(compgen -W "$(__suggest_ord_soumet_candidates)" -- ${cur}))

	return 0
}

__suggest_ord_soumet_candidates(){
	# We use the current word to decide what to do
	local cur="${COMP_WORDS[COMP_CWORD]}"
	if __ord_soumet_dash_dash_in_words ; then
		return
	fi

	option=$(__ord_soumet_get_current_option)
	if [[ "$option" != "" ]] ; then
		__suggest_ord_soumet_args_for_option ${option}
	else
		if [[ "$cur" = -* ]] ; then
			__suggest_ord_soumet_options
		fi
	fi

	echo "$candidates"
}

__ord_soumet_dash_dash_in_words(){
	for ((i=0;i<COMP_CWORD-1;i++)) ; do
		w=${COMP_WORD[$i]}
		if [[ "$w" == "--" ]] ; then
			return 0
		fi
	done
	return 1
}

__ord_soumet_get_current_option(){
	# The word before that
	local prev="${COMP_WORDS[COMP_CWORD-1]}"
	if [[ "$prev" == -* ]] ; then
		echo "$prev"
	fi
}

__suggest_ord_soumet_options(){
	candidates=" -addstep -altcfgdir -args -as -c -clone -cm -coschedule -cpus -custom -display -e -epilog -firststep -geom -image -immediate -iojob -jn -jobcfg -jobtar -keep -l -laststep -m -mach -mpi -node -noendwrap -norerun -norset -nosubmit -notify -o -op -p -postfix -preempt -prefix -prio -project -prolog -q -queue -rerun -resid -rsrc -retries -seqno -share -shell -smt -splitstd -sq -ssmuse -sys -t -tag -threads -tmpfs -v -w -waste -with -wrapdir -xterm"
}

__suggest_ord_soumet_args_for_option(){
	case "$1" in
		-addstep) __suggest_ord_soumet_key_addstep_values ;;
		-altcfgdir) __suggest_ord_soumet_key_altcfgdir_values ;;
		-args) __suggest_ord_soumet_key_args_values ;;
		-as) __suggest_ord_soumet_key_as_values ;;
		-c) __suggest_ord_soumet_key_c_values ;;
		-clone) __suggest_ord_soumet_key_clone_values ;;
		-cm) __suggest_ord_soumet_key_cm_values ;;
		-coschedule) __suggest_ord_soumet_key_coschedule_values ;;
		-cpus) __suggest_ord_soumet_key_cpus_values ;;
		-custom) __suggest_ord_soumet_key_custom_values ;;
		-display) __suggest_ord_soumet_key_display_values ;;
		-e) __suggest_ord_soumet_key_e_values ;;
		-epilog) __suggest_ord_soumet_key_epilog_values ;;
		-firststep) __suggest_ord_soumet_key_firststep_values ;;
		-geom) __suggest_ord_soumet_key_geom_values ;;
		-image) __suggest_ord_soumet_key_image_values ;;
		-immediate) __suggest_ord_soumet_key_immediate_values ;;
		-iojob) __suggest_ord_soumet_key_iojob_values ;;
		-jn) __suggest_ord_soumet_key_jn_values ;;
		-jobcfg) __suggest_ord_soumet_key_jobcfg_values ;;
		-jobtar) __suggest_ord_soumet_key_jobtar_values ;;
		-keep) __suggest_ord_soumet_key_keep_values ;;
		-l) __suggest_ord_soumet_key_l_values ;;
		-laststep) __suggest_ord_soumet_key_laststep_values ;;
		-m) __suggest_ord_soumet_key_m_values ;;
		-mach) __suggest_ord_soumet_key_mach_values ;;
		-mpi) __suggest_ord_soumet_key_mpi_values ;;
		-node) __suggest_ord_soumet_key_node_values ;;
		-noendwrap) __suggest_ord_soumet_key_noendwrap_values ;;
		-norerun) __suggest_ord_soumet_key_norerun_values ;;
		-norset) __suggest_ord_soumet_key_norset_values ;;
		-nosubmit) __suggest_ord_soumet_key_nosubmit_values ;;
		-notify) __suggest_ord_soumet_key_notify_values ;;
		-o) __suggest_ord_soumet_key_o_values ;;
		-op) __suggest_ord_soumet_key_op_values ;;
		-p) __suggest_ord_soumet_key_p_values ;;
		-postfix) __suggest_ord_soumet_key_postfix_values ;;
		-preempt) __suggest_ord_soumet_key_preempt_values ;;
		-prefix) __suggest_ord_soumet_key_prefix_values ;;
		-prio) __suggest_ord_soumet_key_prio_values ;;
		-project) __suggest_ord_soumet_key_project_values ;;
		-prolog) __suggest_ord_soumet_key_prolog_values ;;
		-q) __suggest_ord_soumet_key_q_values ;;
		-queue) __suggest_ord_soumet_key_queue_values ;;
		-rerun) __suggest_ord_soumet_key_rerun_values ;;
		-resid) __suggest_ord_soumet_key_resid_values ;;
		-rsrc) __suggest_ord_soumet_key_rsrc_values ;;
		-retries) __suggest_ord_soumet_key_retries_values ;;
		-seqno) __suggest_ord_soumet_key_seqno_values ;;
		-share) __suggest_ord_soumet_key_share_values ;;
		-shell) __suggest_ord_soumet_key_shell_values ;;
		-smt) __suggest_ord_soumet_key_smt_values ;;
		-splitstd) __suggest_ord_soumet_key_splitstd_values ;;
		-sq) __suggest_ord_soumet_key_sq_values ;;
		-ssmuse) __suggest_ord_soumet_key_ssmuse_values ;;
		-sys) __suggest_ord_soumet_key_sys_values ;;
		-t) __suggest_ord_soumet_key_t_values ;;
		-tag) __suggest_ord_soumet_key_tag_values ;;
		-threads) __suggest_ord_soumet_key_threads_values ;;
		-tmpfs) __suggest_ord_soumet_key_tmpfs_values ;;
		-v) __suggest_ord_soumet_key_v_values ;;
		-w) __suggest_ord_soumet_key_w_values ;;
		-waste) __suggest_ord_soumet_key_waste_values ;;
		-with) __suggest_ord_soumet_key_with_values ;;
		-wrapdir) __suggest_ord_soumet_key_wrapdir_values ;;
		-xterm) __suggest_ord_soumet_key_xterm_values ;;
	esac
}

__suggest_ord_soumet_key_addstep_values(){
	candidates=""
}

__suggest_ord_soumet_key_altcfgdir_values(){
	candidates=""
}

__suggest_ord_soumet_key_args_values(){
	candidates=""
}

__suggest_ord_soumet_key_as_values(){
	candidates=""
}

__suggest_ord_soumet_key_c_values(){
	candidates=""
}

__suggest_ord_soumet_key_clone_values(){
	candidates=""
}

__suggest_ord_soumet_key_cm_values(){
	candidates=""
}

__suggest_ord_soumet_key_coschedule_values(){
	candidates=""
}

__suggest_ord_soumet_key_cpus_values(){
	candidates=""
}

__suggest_ord_soumet_key_custom_values(){
	candidates=""
}

__suggest_ord_soumet_key_display_values(){
	candidates=""
}

__suggest_ord_soumet_key_e_values(){
	candidates=""
}

__suggest_ord_soumet_key_epilog_values(){
	candidates=""
}

__suggest_ord_soumet_key_firststep_values(){
	candidates=""
}

__suggest_ord_soumet_key_geom_values(){
	candidates=""
}

__suggest_ord_soumet_key_image_values(){
	candidates=""
}

__suggest_ord_soumet_key_immediate_values(){
	candidates=""
}

__suggest_ord_soumet_key_iojob_values(){
	candidates=""
}

__suggest_ord_soumet_key_jn_values(){
	candidates=""
}

__suggest_ord_soumet_key_jobcfg_values(){
	candidates=""
}

__suggest_ord_soumet_key_jobtar_values(){
	candidates=""
}

__suggest_ord_soumet_key_keep_values(){
	candidates=""
}

__suggest_ord_soumet_key_l_values(){
	candidates=""
}

__suggest_ord_soumet_key_laststep_values(){
	candidates=""
}

__suggest_ord_soumet_key_m_values(){
	candidates=""
}

__suggest_ord_soumet_key_mach_values(){
	candidates=""
}

__suggest_ord_soumet_key_mpi_values(){
	candidates=""
}

__suggest_ord_soumet_key_node_values(){
	candidates=""
}

__suggest_ord_soumet_key_noendwrap_values(){
	candidates=""
}

__suggest_ord_soumet_key_norerun_values(){
	candidates=""
}

__suggest_ord_soumet_key_norset_values(){
	candidates=""
}

__suggest_ord_soumet_key_nosubmit_values(){
	candidates=""
}

__suggest_ord_soumet_key_notify_values(){
	candidates=""
}

__suggest_ord_soumet_key_o_values(){
	candidates=""
}

__suggest_ord_soumet_key_op_values(){
	candidates=""
}

__suggest_ord_soumet_key_p_values(){
	candidates=""
}

__suggest_ord_soumet_key_postfix_values(){
	candidates=""
}

__suggest_ord_soumet_key_preempt_values(){
	candidates=""
}

__suggest_ord_soumet_key_prefix_values(){
	candidates=""
}

__suggest_ord_soumet_key_prio_values(){
	candidates=""
}

__suggest_ord_soumet_key_project_values(){
	candidates=""
}

__suggest_ord_soumet_key_prolog_values(){
	candidates=""
}

__suggest_ord_soumet_key_q_values(){
	candidates=""
}

__suggest_ord_soumet_key_queue_values(){
	candidates=""
}

__suggest_ord_soumet_key_rerun_values(){
	candidates=""
}

__suggest_ord_soumet_key_resid_values(){
	candidates=""
}

__suggest_ord_soumet_key_rsrc_values(){
	candidates=""
}

__suggest_ord_soumet_key_retries_values(){
	candidates=""
}

__suggest_ord_soumet_key_seqno_values(){
	candidates=""
}

__suggest_ord_soumet_key_share_values(){
	candidates=""
}

__suggest_ord_soumet_key_shell_values(){
	candidates=""
}

__suggest_ord_soumet_key_smt_values(){
	candidates=""
}

__suggest_ord_soumet_key_splitstd_values(){
	candidates=""
}

__suggest_ord_soumet_key_sq_values(){
	candidates=""
}

__suggest_ord_soumet_key_ssmuse_values(){
	candidates=""
}

__suggest_ord_soumet_key_sys_values(){
	candidates=""
}

__suggest_ord_soumet_key_t_values(){
	candidates=""
}

__suggest_ord_soumet_key_tag_values(){
	candidates=""
}

__suggest_ord_soumet_key_threads_values(){
	candidates=""
}

__suggest_ord_soumet_key_tmpfs_values(){
	candidates=""
}

__suggest_ord_soumet_key_v_values(){
	candidates=""
}

__suggest_ord_soumet_key_w_values(){
	candidates=""
}

__suggest_ord_soumet_key_waste_values(){
	candidates=""
}

__suggest_ord_soumet_key_with_values(){
	candidates=""
}

__suggest_ord_soumet_key_wrapdir_values(){
	candidates=""
}

__suggest_ord_soumet_key_xterm_values(){
	candidates=""
}

complete -o default -F __complete_ord_soumet ord_soumet
